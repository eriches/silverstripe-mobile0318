<?php

namespace Hestec\Mobile0318;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;

class Device extends DataObject {

    private static $singular_name = 'Device';
    private static $plural_name = 'Devices';

    private static $table_name = 'Mobile0318Device';

    private static $db = array(
        'DeviceId' => 'Int',
        'DeviceLink' => 'Varchar(100)',
        'BrandId' => 'Int',
        'Brand' => 'Varchar(100)',
        'Name' => 'Varchar(100)',
        'Os' => 'Varchar(100)',
        'ScreenSize' => 'Decimal',
        'CameraResolution' => 'Int',
        'InternalMemory' => 'Int',
        'WirelessCharging' => 'Boolean',
        'Has4G' => 'Boolean',
        'Has5G' => 'Boolean',
        'Status' => 'Boolean',
        'SubFromPrice' => 'Currency',
        'SubMaxPrice' => 'Currency',
        'MinMb' => 'Int',
        'DeviceMaxPrice' => 'Currency',
        'DeviceImage' => 'Text',
        'DeviceImageLocal' => 'Varchar(255)',
        'ClickScore' => 'Int',
        'DeviceUrl' => 'Varchar(100)',
        'BrandUrl' => 'Varchar(100)',
        'ShowOnWebsite' => 'Boolean',
        'StillInApi' => 'Boolean'
    );

    private static $defaults = array(
        'ShowOnWebsite' => true
    );

    private static $default_sort='ClickScore DESC, SubFromPrice ASC, ID DESC';

    private static $summary_fields = array(
        'ID' => 'ID',
        'Brand' => 'Brand',
        'Name' => 'Name',
        'ClickScore' => 'Score',
        'Status.Nice' => 'Status',
        'ShowOnWebsite.Nice' => 'Show'
    );

    public function getCMSFields() {

        $ShowOnWebsiteField = CheckboxField::create('ShowOnWebsite', "ShowOnWebsite");

        return new FieldList(
            $ShowOnWebsiteField
        );

    }

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return "€ ".$output;

    }

}