<?php

namespace Hestec\Mobile0318;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldPrintButton;
use SilverStripe\Forms\GridField\GridFieldImportButton;
use SilverStripe\Forms\GridField\GridFieldExportButton;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use SilverStripe\Forms\GridField\GridFieldPageCount;

class Mobile0318Admin extends ModelAdmin {

    private static $managed_models = array(
        Device::class
    );

    // disable the importer
    private static $model_importers = array();

    // Linked as /admin/slides/
    private static $url_segment = 'mobile0318';

    // title in cms navigation
    private static $menu_title = 'Mobile0318';

    // menu icon
    private static $menu_icon = '_resources/vendor/hestec/silverstripe-mobile0318/client/images/icons/mobile.png';

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);

        $gridfield = $form->Fields()
            ->dataFieldByName($this->sanitiseClassName($this->modelClass));

        $gridfieldConfig = $gridfield->getConfig();

        $gridfieldConfig->removeComponentsByType(GridFieldDeleteAction::class);
        $gridfieldConfig->removeComponentsByType(GridFieldAddNewButton::class);
        $gridfieldConfig->removeComponentsByType(GridFieldPrintButton::class);
        $gridfieldConfig->removeComponentsByType(GridFieldImportButton::class);
        $gridfieldConfig->removeComponentsByType(GridFieldExportButton::class);
        $gridfieldConfig->removeComponentsByType(GridFieldPaginator::class);
        $gridfieldConfig->removeComponentsByType(GridFieldPageCount::class);

        return $form;
    }

}
