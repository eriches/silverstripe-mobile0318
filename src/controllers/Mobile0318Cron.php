<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;
use Hestec\Mobile0318\Device;
use Hestec\LinkManager\Click;

class Mobile0318Cron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'UpdateDevices',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            /*$whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');*/

            $data = array();
            $data['apikey'] = "72878103c0a9f0610b031d79054eb13991e126b0bcfd8c54a3e4148a0ee49889";
            $data_string = json_encode($data);

            return $data_string;

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'https://mobiel.0318media.nl/api/whitelabel/devices', [
                /*'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ],*/
                'body' => $data_string,
                'headers' => [
                    'Content-Type' => 'application/json'
                    //'Content-Length' => strlen($data_string),
                ]
            ]);
            /*$msgs = json_decode($response->getBody());


            foreach ($msgs->products as $node) {

                //echo $node->score_elements;

                foreach ($node->score_elements as $node) {

                    echo $node->type;

                }

            }*/

            return $response->getBody();

            /*$data = array();
            $data['apikey'] = "72878103c0a9f0610b031d79054eb13991e126b0bcfd8c54a3e4148a0ee49889";
$data_string = json_encode($data);
$ch = curl_init('https://mobiel.0318media.nl/api/whitelabel/devices');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')
);
return curl_exec($ch);*/
            //return "hi";

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function UpdateDevices() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $mobile0318_apikey = Config::inst()->get('AffiliateDetails', 'mobile0318_apikey');

            $data = array();
            $data['apikey'] = $mobile0318_apikey;
            $data_string = json_encode($data);

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('POST', 'https://mobiel.0318media.nl/api/whitelabel/devices', [
                    'body' => $data_string,
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ]
                ]);
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Mobile0318 devices", $e->getMessage());
                $email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                DB::query("UPDATE Mobile0318Device SET StillInApi = 0");

                foreach ($msgs as $node) {

                    $count = Device::get()->filter('DeviceId', $node->device_id)->count();

                    if ($count == 0) {

                        $dev = new Device();

                    } else {

                        $dev = Device::get()->filter('DeviceId', $node->device_id)->first();

                    }



                    if ($dev != Device::get()->filter('DeviceId', $node->device_id)->first()) {

                        $dev = new Device();

                    }

                    $dev->StillInApi = 1; // see the DB::query above
                    $dev->DeviceId = $node->device_id;
                    $dev->DeviceLink = $node->device_devicelink;
                    $dev->BrandId = $node->device_brand_id;
                    $dev->Brand = $node->device_brand;
                    $dev->Name = $node->device_name;
                    $dev->Os = $node->device_os;
                    $dev->ScreenSize = $node->device_screensize;
                    $dev->CameraResolution = $node->device_cameraresolution;
                    $dev->InternalMemory = $node->device_internalmemory;
                    $dev->WirelessCharging = $node->device_wirelesscharging;
                    $dev->Has4G = $node->device_4g;
                    $dev->Has5G = $node->device_5g;
                    $dev->Status = $node->device_status;
                    $dev->SubFromPrice = $node->device_vanafprijs_abo;
                    $dev->SubMaxPrice = $node->device_max_aboprijs;
                    $dev->MinMb = $node->device_min_mb;
                    $dev->DeviceMaxPrice = $node->device_max_toestelprijs;
                    $dev->DeviceImage = $node->device_cdn_image;

                    $fileparts = pathinfo($node->device_cdn_image);
                    $filename = SiteTree::create()->generateURLSegment($node->device_brand."-".$node->device_devicelink);

                    $dev->DeviceUrl = $filename;
                    $dev->BrandUrl = SiteTree::create()->generateURLSegment($node->device_brand);

                    $dev->DeviceImageLocal = $filename.".".$fileparts['extension'];

                    //score
                    $clickdate = new \DateTime();
                    $clickdate->modify('-3 months');

                    $clicks = Click::get()->filter(array('Extra' => "0318_".$node->device_devicelink, 'ClickDate:GreaterThan' => $clickdate->format('Y-m-d')))->count();

                    $dev->ClickScore = $clicks;
                    $dev->write();

                    $img = $_SERVER["DOCUMENT_ROOT"]."/themes/main/img/devices/".$filename.".".$fileparts['extension'];
                    if (!file_exists($img)){
                        file_put_contents($img, file_get_contents($node->device_cdn_image));
                    }

                }
            }

            // delete out of api records
            $devices = Device::get()->filter('StillInApi', false);

            foreach ($devices as $device) {

                $device->delete();

            }

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

}
